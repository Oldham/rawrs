# Instructions


The instructions on this page were copied from [here.](https://en.wikibooks.org/wiki/MIPS_Assembly)

## Table


<p id='instruction-table-p'></p>

| Instruction | Description                                                                                                   |
|-------------|---------------------------------------------------------------------------------------------------------------|
| li          | Load Immediate: The li pseudo instruction loads an immediate value into a register\. e\.g\. \`li $8, 0x3BF20` |
| la          | Load Address: Loads the address into a register\. e\.g\. \`la $a0,address`                                    |
