# Contributors

* wilkie
* Luís Oliveira
* jason
* Austin Oldham
* Nate Stump

## Documentation

* wilkie

## Artwork

* Maxicons - Dinosaurs (CCBY, licensed)
* wilkie - RAWRS Icon

## TinyEMU

* Fabrice Bellard
* wilkie (modifications)
